#ifndef PIPERVOICE_H
#define PIPERVOICE_H

#include "speechvoice.h"

class PiperVoice : public SpeechVoice
{
    Q_OBJECT

public:
    PiperVoice();
    ~PiperVoice();
    virtual QString voiceCommand() = 0;

    protected:
    void  performSpeak(QString filename, QString text) = 0;
    void cancel() = 0;
    QString installationPath() = 0;
    virtual QString getName() = 0;
    QProcess *process;
    QProcess *createWavProcess;
};

class LessacLowPiperVoice : public PiperVoice
{
    QString voiceCommand()
    {
        return "/usr/share/omilo-qt5/piper/voices/en_US-lessac-low.onnx";
    }

    void performSpeak(QString filename, QString text) {
        cancelled = false;
        this->filename = filename;
#ifdef Q_OS_WIN
        text.replace("\"", "");
        text.replace("\n", "");
        text.replace("\r", "");

        process->setStandardOutputProcess(createWavProcess);
        process->start("cmd.exe", QStringList() << "/C" << "echo " << text);

        QString command =  "\"" + QDir::currentPath() + "\\piper\\piper.exe\"";
        QString voiceCommand = "en_US-lessac-low.onnx";
        QString voiceLocation = QDir::currentPath() + "\\piper\\voices\\" + voiceCommand;

        QStringList arguments;
        arguments << "-m" << voiceLocation << "-f" << filename;

        createWavProcess->start(command, arguments);
        process->waitForFinished();
#else
        QString command = "echo \""  + text + "\" | /usr/share/omilo-qt5/piper/piper --model /usr/share/omilo-qt5/piper/voices/en_US-lessac-low.onnx -f " +  filename;
        createWavProcess->start("sh", QStringList() << "-c" << command);
#endif
    }

    QString getName()
    {
        return LessacLowPiper;
    }
    QString getLanguage()
    {
        return QLocale::languageToString(QLocale::English);
    }

    QString installationPath()
    {
        return "/usr/share/omilo-qt5/piper/voices/en_US-lessac-low.onnx";
    }

    void cancel() {
        cancelled = true;
        process->close();
        createWavProcess->close();
    }
};

#endif // PIPERVOICE_H

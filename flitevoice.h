#ifndef FLITEVOICE_H
#define FLITEVOICE_H

#include "festivalflitevoice.h"
#include "definitions.h"

class FliteVoice : public FestivalFliteVoice
{
public:
    void performSpeak(QString filename, QString text);
    QString installationPath();
};

class RmsFliteVoice : public FliteVoice
{
public:
    RmsFliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean =  defTargetMean;
    }
    QString getName()
    {
        return RmsFlite;
    }
private:
    QString voiceCommand()
    {
        return "-voice rms";
    }
};

class SltFliteVoice : public FliteVoice
{
public:
    SltFliteVoice()
    {
        this->durationStretch = defDurationStretch;
        this->targetMean = defTargetMean;
    }
    QString getName()
    {
        return SltFlite;
   }
private:
    QString voiceCommand()
    {
        return "-voice slt";
    }
};

class AwbFliteVoice : public FliteVoice
{
public:
    AwbFliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return AwbFlite;
    }
private:
    QString voiceCommand()
    {
        return "-voice awb";
    }
};

class Kal16FliteVoice : public FliteVoice
{
public:
    Kal16FliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return Kal16Flite;
    }
private:
    QString voiceCommand()
    {
        return "-voice kal16";
    }
};

class IndicBenRmFliteVoice : public FliteVoice
{
public:
    IndicBenRmFliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return IndicBenRmFlite;
    }
private:
    QString voiceCommand()
    {
        return "-voice /usr/share/omilo-qt5/flite/voices/cmu_indic_ben_rm.flitevox";
    }
};

class IndicGujAdFliteVoice : public FliteVoice
{
public:
    IndicGujAdFliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return IndicGujAdFlite;
    }
private:
    QString voiceCommand()
    {
        return "-voice /usr/share/omilo-qt5/flite/voices/cmu_indic_guj_ad.flitevox";
    }
};

class IndicGujDpFliteVoice : public FliteVoice
{
public:
    IndicGujDpFliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return IndicGujDpFlite;
    }
private:
    QString voiceCommand()
    {
        return "-voice /usr/share/omilo-qt5/flite/voices/cmu_indic_guj_dp.flitevox";
    }
};

class IndicGujKtFliteVoice : public FliteVoice
{
public:
    IndicGujKtFliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return IndicGujKtFlite;
    }
private:
    QString voiceCommand()
    {
        return "-voice /usr/share/omilo-qt5/flite/voices/cmu_indic_guj_kt.flitevox";
    }
};

class IndicHinAbFliteVoice : public FliteVoice
{
public:
    IndicHinAbFliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return IndicHinAbFlite;
    }
private:
    QString voiceCommand()
    {
        return "-voice /usr/share/omilo-qt5/flite/voices/cmu_indic_hin_ab.flitevox";
    }
};

class IndicKanPlvFliteVoice : public FliteVoice
{
public:
    IndicKanPlvFliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return IndicKanPlvFlite;
    }
private:
    QString voiceCommand()
    {
        return "-voice /usr/share/omilo-qt5/flite/voices/cmu_indic_kan_plv.flitevox";
    }
};

class IndicMarAupFliteVoice : public FliteVoice
{
public:
    IndicMarAupFliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return IndicMarAupFlite;
    }
private:
    QString voiceCommand()
    {
        return "-voice /usr/share/omilo-qt5/flite/voices/cmu_indic_mar_aup.flitevox";
    }
};

class IndicMarSlpFliteVoice : public FliteVoice
{
public:
    IndicMarSlpFliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return IndicMarSlpFlite;
    }
private:
    QString voiceCommand()
    {
        return "-voice /usr/share/omilo-qt5/flite/voices/cmu_indic_mar_slp.flitevox";
    }
};

class IndicPanAmpFliteVoice : public FliteVoice
{
public:
    IndicPanAmpFliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return IndicPanAmpFlite;
    }
private:
    QString voiceCommand()
    {
        return "-voice /usr/share/omilo-qt5/flite/voices/cmu_indic_pan_amp.flitevox";
    }
};

class IndicTamSdrFliteVoice : public FliteVoice
{
public:
    IndicTamSdrFliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return IndicTamSdrFlite;
    }
private:
    QString voiceCommand()
    {
        return "-voice /usr/share/omilo-qt5/flite/voices/cmu_indic_tam_sdr.flitevox";
    }
};

class IndicTelKpnFliteVoice : public FliteVoice
{
public:
    IndicTelKpnFliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return IndicTelKpnFlite;
    }
private:
    QString voiceCommand()
    {
        return "-voice /usr/share/omilo-qt5/flite/voices/cmu_indic_tel_kpn.flitevox";
    }
};

class IndicTelSkFliteVoice : public FliteVoice
{
public:
    IndicTelSkFliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return IndicTelSkFlite;
    }
private:
    QString voiceCommand()
    {
        return "-voice /usr/share/omilo-qt5/flite/voices/cmu_indic_tel_sk.flitevox";
    }
};

class IndicTelSsFliteVoice : public FliteVoice
{
public:
    IndicTelSsFliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return IndicTelSsFlite;
    }
private:
    QString voiceCommand()
    {
        return "-voice /usr/share/omilo-qt5/flite/voices/cmu_indic_tel_ss.flitevox";
    }
};

class AewFliteVoice : public FliteVoice
{
public:
    AewFliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return AewFlite;
    }
private:
    QString voiceCommand()
    {
        return "-voice /usr/share/omilo-qt5/flite/voices/cmu_us_aew.flitevox";
    }
};

class AhwFliteVoice : public FliteVoice
{
public:
    AhwFliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return AhwFlite;
    }
private:
    QString voiceCommand()
    {
        return "-voice /usr/share/omilo-qt5/flite/voices/cmu_us_ahw.flitevox";
    }
};

class AupFliteVoice : public FliteVoice
{
public:
    AupFliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return AupFlite;
    }
private:
    QString voiceCommand()
    {
        return "-voice /usr/share/omilo-qt5/flite/voices/cmu_us_aup.flitevox";
    }
};

class Awb2FliteVoice : public FliteVoice
{
public:
    Awb2FliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return Awb2Flite;
    }
private:
    QString voiceCommand()
    {
        return "-voice /usr/share/omilo-qt5/flite/voices/cmu_us_awb.flitevox";
    }
};

class AxbFliteVoice : public FliteVoice
{
public:
    AxbFliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return AxbFlite;
    }
private:
    QString voiceCommand()
    {
        return "-voice /usr/share/omilo-qt5/flite/voices/cmu_us_axb.flitevox";
    }
};

class BdlFliteVoice : public FliteVoice
{
public:
    BdlFliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return BdlFlite;
    }
private:
    QString voiceCommand()
    {
        return "-voice /usr/share/omilo-qt5/flite/voices/cmu_us_bdl.flitevox";
    }
};

class ClbFliteVoice : public FliteVoice
{
public:
    ClbFliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return ClbFlite;
    }
private:
    QString voiceCommand()
    {
        return "-voice /usr/share/omilo-qt5/flite/voices/cmu_us_clb.flitevox";
    }
};

class EeyFliteVoice : public FliteVoice
{
public:
    EeyFliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return EeyFlite;
    }
private:
    QString voiceCommand()
    {
        return "-voice /usr/share/omilo-qt5/flite/voices/cmu_us_eey.flitevox";
    }
};

class FemFliteVoice : public FliteVoice
{
public:
    FemFliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return FemFlite;
    }
private:
    QString voiceCommand()
    {
        return "-voice /usr/share/omilo-qt5/flite/voices/cmu_us_fem.flitevox";
    }
};

class GkaFliteVoice : public FliteVoice
{
public:
    GkaFliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return FemFlite;
    }
private:
    QString voiceCommand()
    {
        return "-voice /usr/share/omilo-qt5/flite/voices/cmu_us_gka.flitevox";
    }
};

class JmkFliteVoice : public FliteVoice
{
public:
    JmkFliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return JmkFlite;
    }
private:
    QString voiceCommand()
    {
        return "-voice /usr/share/omilo-qt5/flite/voices/cmu_us_jmk.flitevox";
    }
};

class KspFliteVoice : public FliteVoice
{
public:
    KspFliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return KspFlite;
    }
private:
    QString voiceCommand()
    {
        return "-voice /usr/share/omilo-qt5/flite/voices/cmu_us_ksp.flitevox";
    }
};

class LjmFliteVoice : public FliteVoice
{
public:
    LjmFliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return LjmFlite;
    }
private:
    QString voiceCommand()
    {
        return "-voice /usr/share/omilo-qt5/flite/voices/cmu_us_ljm.flitevox";
    }
};

class LnhFliteVoice : public FliteVoice
{
public:
    LnhFliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return LnhFlite;
    }
private:
    QString voiceCommand()
    {
        return "-voice /usr/share/omilo-qt5/flite/voices/cmu_us_lnh.flitevox";
    }
};

class Rms2FliteVoice : public FliteVoice
{
public:
    Rms2FliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return Rms2Flite;
    }
private:
    QString voiceCommand()
    {
        return "-voice /usr/share/omilo-qt5/flite/voices/cmu_us_rms.flitevox";
    }
};

class RxrFliteVoice : public FliteVoice
{
public:
    RxrFliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return RxrFlite;
    }
private:
    QString voiceCommand()
    {
        return "-voice /usr/share/omilo-qt5/flite/voices/cmu_us_rxr.flitevox";
    }
};

class SlpFliteVoice : public FliteVoice
{
public:
    SlpFliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return SlpFlite;
    }
private:
    QString voiceCommand()
    {
        return "-voice /usr/share/omilo-qt5/flite/voices/cmu_us_slp.flitevox";
    }
};

class Slt2FliteVoice : public FliteVoice
{
public:
    Slt2FliteVoice()
    {
        durationStretch = defDurationStretch;
        targetMean = defTargetMean;
    }
    QString getName()
    {
        return Slt2Flite;
    }
private:
    QString voiceCommand()
    {
        return "-voice /usr/share/omilo-qt5/flite/voices/cmu_us_slt.flitevox";
    }
};

#endif // FLITEVOICE_H

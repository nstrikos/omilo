#ifndef FESTIVALVOICE_H
#define FESTIVALVOICE_H

#include "festivalflitevoice.h"

class FestivalVoice : public FestivalFliteVoice
{
public:
    void performSpeak(QString filename, QString text);
};

class KalDiphoneFestivalVoice : public FestivalVoice
{
public:
    QString getName()
    {
        return KalFestival;
    }    
private:
    QString voiceCommand()
    {
        return "";
    }
    QString installationPath()
    {
        return "/usr/share/festival/voices/english/kal_diphone";
    }
};

class AwbCmuFestivalVoice : public FestivalVoice
{
public:
    QString getName()
    {
         return AwbCmuFestival;
    }
private:
    QString voiceCommand()
    {
        return " -eval \"(voice_cmu_us_awb_cg)\"";
    }
    QString installationPath()
    {
        return  "/usr/share/festival/voices/us/cmu_us_awb_cg";
    }
};


class AewCmuFestivalVoice : public FestivalVoice
{
public:
    QString getName()
    {
        return AewCmuFestival;
    }
private:
    QString voiceCommand()
    {
        return " -eval \"(voice_cmu_us_aew_cg)\"";
    }
    QString installationPath()
    {
        return  "/usr/share/festival/voices/us/cmu_us_aew_cg";
    }
};

class AhwCmuFestivalVoice : public FestivalVoice
{
public:
    QString getName()
    {
        return AhwCmuFestival;
    }
private:
    QString voiceCommand()
    {
        return " -eval \"(voice_cmu_us_ahw_cg)\"";
    }
    QString installationPath()
    {
        return  "/usr/share/festival/voices/us/cmu_us_ahw_cg";
    }
};

class AupCmuFestivalVoice : public FestivalVoice
{
public:
    QString getName()
    {
        return AupCmuFestival;
    }
private:
    QString voiceCommand()
    {
        return " -eval \"(voice_cmu_us_aup_cg)\"";
    }
    QString installationPath()
    {
        return  "/usr/share/festival/voices/us/cmu_us_aup_cg";
    }
};

class AxbCmuFestivalVoice : public FestivalVoice
{
public:
    QString getName()
    {
        return AxbCmuFestival;
    }
private:
    QString voiceCommand()
    {
        return " -eval \"(voice_cmu_us_axb_cg)\"";
    }
    QString installationPath()
    {
        return  "/usr/share/festival/voices/us/cmu_us_axb_cg";
    }
};

class BdlCmuFestivalVoice : public FestivalVoice
{
public:
    QString getName()
    {
        return BdlCmuFestival;
    }
private:
    QString voiceCommand()
    {
          return " -eval \"(voice_cmu_us_bdl_cg)\"";
    }
    QString installationPath()
    {
         return  "/usr/share/festival/voices/us/cmu_us_bdl_cg";
    }
};

class ClbCmuFestivalVoice : public FestivalVoice
{
public:
    QString getName()
    {
        return ClbCmuFestival;
    }
private:
    QString voiceCommand()
    {
         return " -eval \"(voice_cmu_us_clb_cg)\"";
    }
    QString installationPath()
    {
        return  "/usr/share/festival/voices/us/cmu_us_clb_cg";
    }
};

class EeyCmuFestivalVoice : public FestivalVoice
{
public:
    QString getName()
    {
        return EeyCmuFestival;
    }
private:
    QString voiceCommand()
    {
        return " -eval \"(voice_cmu_us_eey_cg)\"";
    }
    QString installationPath()
    {
        return  "/usr/share/festival/voices/us/cmu_us_eey_cg";
    }
};

class FemCmuFestivalVoice : public FestivalVoice
{
public:
    QString getName()
    {
        return FemCmuFestival;
    }
private:
    QString voiceCommand()
    {
        return " -eval \"(voice_cmu_us_fem_cg)\"";
    }
    QString installationPath()
    {
        return  "/usr/share/festival/voices/us/cmu_us_fem_cg";
    }
};

class GkaCmuFestivalVoice : public FestivalVoice
{
public:
    QString getName()
    {
        return GkaCmuFestival;
    }
private:
    QString voiceCommand()
    {
        return " -eval \"(voice_cmu_us_gka_cg)\"";
    }
    QString installationPath()
    {
        return  "/usr/share/festival/voices/us/cmu_us_gka_cg";
    }
};

class JmkCmuFestivalVoice : public FestivalVoice
{
public:
    QString getName()
    {
         return JmkCmuFestival;
    }
private:
    QString voiceCommand()
    {
        return " -eval \"(voice_cmu_us_jmk_cg)\"";
    }
    QString installationPath()
    {
         return  "/usr/share/festival/voices/us/cmu_us_jmk_cg";
    }
};

class KspCmuFestivalVoice : public FestivalVoice
{
public:
    QString getName()
    {
         return KspCmuFestival;
    }
private:
    QString voiceCommand()
    {
         return " -eval \"(voice_cmu_us_ksp_cg)\"";
    }
    QString installationPath()
    {
         return  "/usr/share/festival/voices/us/cmu_us_ksp_cg";
    }
};

class LjmCmuFestivalVoice : public FestivalVoice
{
public:
    QString getName()
    {
        return LjmCmuFestival;
    }
private:
    QString voiceCommand()
    {
        return " -eval \"(voice_cmu_us_ljm_cg)\"";
    }
    QString installationPath()
    {
        return  "/usr/share/festival/voices/us/cmu_us_ljm_cg";
    }
};

class LnhCmuFestivalVoice : public FestivalVoice
{
public:
    QString getName()
    {
        return LnhCmuFestival;
    }
private:
    QString voiceCommand()
    {
        return " -eval \"(voice_cmu_us_lnh_cg)\"";
    }
    QString installationPath()
    {
        return  "/usr/share/festival/voices/us/cmu_us_lnh_cg";
    }
};

class RmsCmuFestivalVoice : public FestivalVoice
{
public:
    QString getName()
    {
         return RmsCmuFestival;
    }
private:
    QString voiceCommand()
    {
        return " -eval \"(voice_cmu_us_rms_cg)\"";
    }
    QString installationPath()
    {
         return  "/usr/share/festival/voices/us/cmu_us_rms_cg";
    }
};

class RxrCmuFestivalVoice : public FestivalVoice
{
public:
    QString getName()
    {
        return RxrCmuFestival;
    }
private:
    QString voiceCommand()
    {
        return " -eval \"(voice_cmu_us_rxr_cg)\"";
    }
    QString installationPath()
    {
        return  "/usr/share/festival/voices/us/cmu_us_rxr_cg";
    }
};

class SlpCmuFestivalVoice : public FestivalVoice
{
public:
    QString getName()
    {
        return SlpCmuFestival;
    }
private:
    QString voiceCommand()
    {
        return " -eval \"(voice_cmu_us_slp_cg)\"";
    }
    QString installationPath()
    {
        return  "/usr/share/festival/voices/us/cmu_us_slp_cg";
    }
};

class SltCmuFestivalVoice : public FestivalVoice
{
public:
    QString getName()
    {
        return SltCmuFestival;
    }
private:
    QString voiceCommand()
    {
         return " -eval \"(voice_cmu_us_slt_cg)\"";
    }
    QString installationPath()
    {
          return  "/usr/share/festival/voices/us/cmu_us_slt_cg";
    }
};




#endif // FESTIVALVOICE_H

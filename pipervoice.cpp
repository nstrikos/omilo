#include "pipervoice.h"

PiperVoice::PiperVoice()
{
    process = new QProcess();
    createWavProcess = new QProcess();    
    connect(createWavProcess, SIGNAL(finished(int)), this, SLOT(emitFileCreatedSignal()));
}

PiperVoice::~PiperVoice()
{
    process->close();
    delete process;
    createWavProcess->close();
    delete createWavProcess;
}

#include "installvoicesdialog.h"
#include "progressdialog.h"

InstallVoicesDialog::InstallVoicesDialog(QWidget *parent) : QDialog(parent)
{
    setupUi(this);

    myProgressDialog = NULL;
    installationProcess = new QProcess();
    downloadProcess = new QProcess();
    removeProcess = new QProcess();

    engineInfo = new SpeechEngineInfo();

    connect(downloadProcess, SIGNAL(finished(int)), this, SLOT(downloadFinished()));
    connect(installationProcess, SIGNAL(finished(int)), this, SLOT(installationFinished()));
    connect(removeProcess, SIGNAL(finished(int)), this, SLOT(removeFinished()));

    QFile file1("/usr/bin/pkexec");
    QFile file2("/usr/bin/gksu");
    QFile file3("/usr/bin/kdesu");
    QFile file4("/usr/bin/gnomesu");
    if (file1.exists())
        sudoCommand = "pkexec ";
    else if (file2.exists())
        sudoCommand = "gksu ";
    else if (file3.exists())
        sudoCommand = "kdesu ";
    else if (file4.exists())
        sudoCommand = "gnomesu ";

    qDebug() << "Install voices dialog created...";
}

InstallVoicesDialog::~InstallVoicesDialog()
{
    qDebug() << "Deleting install voices dialog...";
    delete installationProcess;
    delete removeProcess;
    delete downloadProcess;
    delete myProgressDialog;
    delete engineInfo;
}

void InstallVoicesDialog::updateAvailableVoices()
{
    unsigned int i;

    engineInfo->update();

    for (i = 0; i < engineInfo->installedVoices.size(); i++)
    {
        //if (engineInfo->installedVoices[i].mode == festivalCmu)
        //{
        if (engineInfo->installedVoices[i].name == AwbCmuFestival)
        {
            awbInstallButton->setEnabled(false);
            awbRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == AewCmuFestival)
        {
            aewInstallButton->setEnabled(false);
            aewRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == AhwCmuFestival)
        {
            ahwInstallButton->setEnabled(false);
            ahwRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == AupCmuFestival)
        {
            aupInstallButton->setEnabled(false);
            aupRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == AxbCmuFestival)
        {
            axbInstallButton->setEnabled(false);
            axbRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == BdlCmuFestival)
        {
            bdlInstallButton->setEnabled(false);
            bdlRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == ClbCmuFestival)
        {
            clbInstallButton->setEnabled(false);
            clbRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == EeyCmuFestival)
        {
            eeyInstallButton->setEnabled(false);
            eeyRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == FemCmuFestival)
        {
            femInstallButton->setEnabled(false);
            femRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == GkaCmuFestival)
        {
            gkaInstallButton->setEnabled(false);
            gkaRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == LjmCmuFestival)
        {
            ljmInstallButton->setEnabled(false);
            ljmRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == LnhCmuFestival)
        {
            lnhInstallButton->setEnabled(false);
            lnhRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == JmkCmuFestival)
        {
            jmkInstallButton->setEnabled(false);
            jmkRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == KspCmuFestival)
        {
            kspInstallButton->setEnabled(false);
            kspRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == RmsCmuFestival)
        {
            rmsInstallButton->setEnabled(false);
            rmsRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == RxrCmuFestival)
        {
            rxrInstallButton->setEnabled(false);
            rxrRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == SlpCmuFestival)
        {
            slpInstallButton->setEnabled(false);
            slpRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == SltCmuFestival)
        {
            sltInstallButton->setEnabled(false);
            sltRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == AewFlite)
        {
            aewFliteInstallButton->setEnabled(false);
            aewFliteRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == AhwFlite)
        {
            ahwFliteInstallButton->setEnabled(false);
            ahwFliteRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == AupFlite)
        {
            aupFliteInstallButton->setEnabled(false);
            aupFliteRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == Awb2Flite)
        {
            awb2FliteInstallButton->setEnabled(false);
            awb2FliteRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == AxbFlite)
        {
            axbFliteInstallButton->setEnabled(false);
            axbFliteRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == BdlFlite)
        {
            bdlFliteInstallButton->setEnabled(false);
            bdlFliteRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == ClbFlite)
        {
            clbFliteInstallButton->setEnabled(false);
            clbFliteRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == EeyFlite)
        {
            eeyFliteInstallButton->setEnabled(false);
            eeyFliteRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == FemFlite)
        {
            femFliteInstallButton->setEnabled(false);
            femFliteRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == GkaFlite)
        {
            gkaFliteInstallButton->setEnabled(false);
            gkaFliteRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == JmkFlite)
        {
            jmkFliteInstallButton->setEnabled(false);
            jmkFliteRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == KspFlite)
        {
            kspFliteInstallButton->setEnabled(false);
            kspFliteRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == LjmFlite)
        {
            ljmFliteInstallButton->setEnabled(false);
            ljmFliteRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == LnhFlite)
        {
            lnhFliteInstallButton->setEnabled(false);
            lnhFliteRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == Rms2Flite)
        {
            rms2FliteInstallButton->setEnabled(false);
            rms2FliteRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == RxrFlite)
        {
            rxrFliteInstallButton->setEnabled(false);
            rxrFliteRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == SlpFlite)
        {
            slpFliteInstallButton->setEnabled(false);
            slpFliteRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == Slt2Flite)
        {
            slt2FliteInstallButton->setEnabled(false);
            slt2FliteRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == IndicBenRmFlite)
        {
            indicBenRmFliteInstallButton->setEnabled(false);
            indicBenRmFliteRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == IndicGujAdFlite)
        {
            IndicGujAdFliteInstallButton->setEnabled(false);
            IndicGujAdFliteRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == IndicGujDpFlite)
        {
            IndicGujDpFliteInstallButton->setEnabled(false);
            IndicGujDpFliteRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == IndicGujKtFlite)
        {
            IndicGujKtFliteInstallButton->setEnabled(false);
            IndicGujKtFliteRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == IndicHinAbFlite)
        {
            IndicHinAbFliteInstallButton->setEnabled(false);
            IndicHinAbFliteRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == IndicKanPlvFlite)
        {
            IndicKanPlvFliteInstallButton->setEnabled(false);
            IndicKanPlvFliteRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == IndicMarAupFlite)
        {
            IndicMarAupFliteInstallButton->setEnabled(false);
            IndicMarAupFliteRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == IndicMarSlpFlite)
        {
            IndicMarSlpFliteInstallButton->setEnabled(false);
            IndicMarSlpFliteRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == IndicPanAmpFlite)
        {
            IndicPanAmpFliteInstallButton->setEnabled(false);
            IndicPanAmpFliteRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == IndicTamSdrFlite)
        {
            IndicTamSdrFliteInstallButton->setEnabled(false);
            IndicTamSdrFliteRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == IndicTelKpnFlite)
        {
            IndicTelKpnFliteInstallButton->setEnabled(false);
            IndicTelKpnFliteRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == IndicTelSkFlite)
        {
            IndicTelSkFliteInstallButton->setEnabled(false);
            IndicTelSkFliteRemoveButton->setEnabled(true);
        }
        else if (engineInfo->installedVoices[i].name == IndicTelSsFlite)
        {
            IndicTelSsFliteInstallButton->setEnabled(false);
            IndicTelSsFliteRemoveButton->setEnabled(true);
        }
        //}
    }

    for (i = 0; i < engineInfo->availableVoices.size(); i++)
    {
        //if (engineInfo->availableVoices[i].mode == festivalCmu)
        //{
        if (engineInfo->availableVoices[i].name == AwbCmuFestival)
        {
            awbInstallButton->setEnabled(true);
            awbRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == BdlCmuFestival)
        {
            bdlInstallButton->setEnabled(true);
            bdlRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == AewCmuFestival)
        {
            aewInstallButton->setEnabled(true);
            aewRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == AhwCmuFestival)
        {
            ahwInstallButton->setEnabled(true);
            ahwRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == AupCmuFestival)
        {
            aupInstallButton->setEnabled(true);
            aupRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == AxbCmuFestival)
        {
            axbInstallButton->setEnabled(true);
            axbRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == ClbCmuFestival)
        {
            clbInstallButton->setEnabled(true);
            clbRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == EeyCmuFestival)
        {
            eeyInstallButton->setEnabled(true);
            eeyRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == FemCmuFestival)
        {
            femInstallButton->setEnabled(true);
            femRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == GkaCmuFestival)
        {
            gkaInstallButton->setEnabled(true);
            gkaRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == LjmCmuFestival)
        {
            ljmInstallButton->setEnabled(true);
            ljmRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == LnhCmuFestival)
        {
            lnhInstallButton->setEnabled(true);
            lnhRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == JmkCmuFestival)
        {
            jmkInstallButton->setEnabled(true);
            jmkRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == KspCmuFestival)
        {
            kspInstallButton->setEnabled(true);
            kspRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == RmsCmuFestival)
        {
            rmsInstallButton->setEnabled(true);
            rmsRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == RxrCmuFestival)
        {
            rxrInstallButton->setEnabled(true);
            rxrRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == SlpCmuFestival)
        {
            slpInstallButton->setEnabled(true);
            slpRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == SltCmuFestival)
        {
            sltInstallButton->setEnabled(true);
            sltRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == AewFlite)
        {
            aewFliteInstallButton->setEnabled(true);
            aewFliteRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == AhwFlite)
        {
            ahwFliteInstallButton->setEnabled(true);
            ahwFliteRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == AupFlite)
        {
            aupFliteInstallButton->setEnabled(true);
            aupFliteRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == Awb2Flite)
        {
            awb2FliteInstallButton->setEnabled(true);
            awb2FliteRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == AxbFlite)
        {
            axbFliteInstallButton->setEnabled(true);
            axbFliteRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == BdlFlite)
        {
            bdlFliteInstallButton->setEnabled(true);
            bdlFliteRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == ClbFlite)
        {
            clbFliteInstallButton->setEnabled(true);
            clbFliteRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == EeyFlite)
        {
            eeyFliteInstallButton->setEnabled(true);
            eeyFliteRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == FemFlite)
        {
            femFliteInstallButton->setEnabled(true);
            femFliteRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == GkaFlite)
        {
            gkaFliteInstallButton->setEnabled(true);
            gkaFliteRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == JmkFlite)
        {
            jmkFliteInstallButton->setEnabled(true);
            jmkFliteRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == KspFlite)
        {
            kspFliteInstallButton->setEnabled(true);
            kspFliteRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == LjmFlite)
        {
            ljmFliteInstallButton->setEnabled(true);
            ljmFliteRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == LnhFlite)
        {
            lnhFliteInstallButton->setEnabled(true);
            lnhFliteRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == Rms2Flite)
        {
            rms2FliteInstallButton->setEnabled(true);
            rms2FliteRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == RxrFlite)
        {
            rxrFliteInstallButton->setEnabled(true);
            rxrFliteRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == SlpFlite)
        {
            slpFliteInstallButton->setEnabled(true);
            slpFliteRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == Slt2Flite)
        {
            slt2FliteInstallButton->setEnabled(true);
            slt2FliteRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == IndicBenRmFlite)
        {
            indicBenRmFliteInstallButton->setEnabled(true);
            indicBenRmFliteRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == IndicGujAdFlite)
        {
            IndicGujAdFliteInstallButton->setEnabled(true);
            IndicGujAdFliteRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == IndicGujDpFlite)
        {
            IndicGujDpFliteInstallButton->setEnabled(true);
            IndicGujDpFliteRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == IndicGujKtFlite)
        {
            IndicGujKtFliteInstallButton->setEnabled(true);
            IndicGujKtFliteRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == IndicHinAbFlite)
        {
            IndicHinAbFliteInstallButton->setEnabled(true);
            IndicHinAbFliteRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == IndicKanPlvFlite)
        {
            IndicKanPlvFliteInstallButton->setEnabled(true);
            IndicKanPlvFliteRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == IndicMarAupFlite)
        {
            IndicMarAupFliteInstallButton->setEnabled(true);
            IndicMarAupFliteRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == IndicMarSlpFlite)
        {
            IndicMarSlpFliteInstallButton->setEnabled(true);
            IndicMarSlpFliteRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == IndicPanAmpFlite)
        {
            IndicPanAmpFliteInstallButton->setEnabled(true);
            IndicPanAmpFliteRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == IndicTamSdrFlite)
        {
            IndicTamSdrFliteInstallButton->setEnabled(true);
            IndicTamSdrFliteRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == IndicTelKpnFlite)
        {
            IndicTelKpnFliteInstallButton->setEnabled(true);
            IndicTelKpnFliteRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == IndicTelSkFlite)
        {
            IndicTelSkFliteInstallButton->setEnabled(true);
            IndicTelSkFliteRemoveButton->setEnabled(false);
        }
        else if (engineInfo->availableVoices[i].name == IndicTelSsFlite)
        {
            IndicTelSsFliteInstallButton->setEnabled(true);
            IndicTelSsFliteRemoveButton->setEnabled(false);
        }
        //}
    }
}

void InstallVoicesDialog::downloadFinished()
{
    //If file exists download was successful. Otherwise download was cancelled
    if (QFile::exists(downloadFilename))
    {
        QString text = tr("Installing...");
        myProgressDialog->textBrowser->setText(text);
        myProgressDialog->progressBar->setValue(100);
        QString command = "sh";
        QString c = "-c";
        QStringList list;
        list << c << sudoCommand + installationCommandPart2;
        installationProcess->start(command, list);
    }
    else
        myProgressDialog->hide();
    myProgressDialog->stopTimer();
}

void InstallVoicesDialog::installationFinished()
{
    myProgressDialog->hide();
    engineInfo->update();
    updateAvailableVoices();
}

void InstallVoicesDialog::removeFinished()
{
    engineInfo->update();
    updateAvailableVoices();
    isSelectedVoiceInstalled(currentVoice);
}

void InstallVoicesDialog::showWindow(QString currentVoice)
{
    updateAvailableVoices();
    this->show();
    this->currentVoice = currentVoice;
}

void InstallVoicesDialog::on_closeButton_clicked()
{
    this->accept();
}

void InstallVoicesDialog::startDownload()
{
    if (!myProgressDialog)
        myProgressDialog = new progressDialog(downloadProcess, downloadFilename, this);
    else
    {
        myProgressDialog->updateProgressDialog(downloadProcess, downloadFilename);
        myProgressDialog->textBrowser->setFocus();
    }
    myProgressDialog->setModal(true);
    QString text = tr("Downloading...\nVoice packages are over 100 Megabytes, this may take a while...\nYour password will be required.");
    myProgressDialog->textBrowser->setText(text);
    myProgressDialog->show();
    downloadProcess->start("sh", QStringList() << "-c" << downloadCommand);
}

void InstallVoicesDialog::on_awbInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_cmu_us_awb_cg.sh";
    downloadFilename = "/tmp/festvox_cmu_us_awb_cg.tar.gz";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/festvox_cmu_us_awb_cg.tar.gz";
    startDownload();
}

void InstallVoicesDialog::on_aewInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_cmu_us_aew_cg.sh";
    downloadFilename = "/tmp/festvox_cmu_us_aew_cg.tar.gz";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/festvox_cmu_us_aew_cg.tar.gz";
    startDownload();
}

void InstallVoicesDialog::on_ahwInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_cmu_us_ahw_cg.sh";
    downloadFilename = "/tmp/festvox_cmu_us_ahw_cg.tar.gz";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/festvox_cmu_us_ahw_cg.tar.gz";
    startDownload();
}

void InstallVoicesDialog::on_aupInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_cmu_us_aup_cg.sh";
    downloadFilename = "/tmp/festvox_cmu_us_aup_cg.tar.gz";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/festvox_cmu_us_aup_cg.tar.gz";
    startDownload();
}

void InstallVoicesDialog::on_axbInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_cmu_us_axb_cg.sh";
    downloadFilename = "/tmp/festvox_cmu_us_axb_cg.tar.gz";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/festvox_cmu_us_axb_cg.tar.gz";
    startDownload();
}

void InstallVoicesDialog::on_bdlInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_cmu_us_bdl_cg.sh";
    downloadFilename = "/tmp/festvox_cmu_us_bdl_cg.tar.gz";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/festvox_cmu_us_bdl_cg.tar.gz";
    startDownload();
}

void InstallVoicesDialog::on_clbInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_cmu_us_clb_cg.sh";
    downloadFilename = "/tmp/festvox_cmu_us_clb_cg.tar.gz";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/festvox_cmu_us_clb_cg.tar.gz";
    startDownload();
}

void InstallVoicesDialog::on_eeyInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_cmu_us_eey_cg.sh";
    downloadFilename = "/tmp/festvox_cmu_us_eey_cg.tar.gz";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/festvox_cmu_us_eey_cg.tar.gz";
    startDownload();
}

void InstallVoicesDialog::on_jmkInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_cmu_us_jmk_cg.sh";
    downloadFilename = "/tmp/festvox_cmu_us_jmk_cg.tar.gz";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/festvox_cmu_us_jmk_cg.tar.gz";
    startDownload();
}

void InstallVoicesDialog::on_kspInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_cmu_us_ksp_cg.sh";
    downloadFilename = "/tmp/festvox_cmu_us_ksp_cg.tar.gz";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/festvox_cmu_us_ksp_cg.tar.gz";
    startDownload();
}

void InstallVoicesDialog::on_rmsInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_cmu_us_rms_cg.sh";
    downloadFilename = "/tmp/festvox_cmu_us_rms_cg.tar.gz";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/festvox_cmu_us_rms_cg.tar.gz";
    startDownload();
}

void InstallVoicesDialog::on_sltInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_cmu_us_slt_cg.sh";
    downloadFilename = "/tmp/festvox_cmu_us_slt_cg.tar.gz";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/festvox_cmu_us_slt_cg.tar.gz";
    startDownload();
}

void InstallVoicesDialog::on_awbRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_cmu_us_awb_cg.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_aewRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_cmu_us_aew_cg.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_ahwRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_cmu_us_ahw_cg.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_aupRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_cmu_us_aup_cg.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_axbRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_cmu_us_axb_cg.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_bdlRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_cmu_us_bdl_cg.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_clbRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_cmu_us_clb_cg.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_eeyRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_cmu_us_eey_cg.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_jmkRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_cmu_us_jmk_cg.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_kspRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_cmu_us_ksp_cg.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_rmsRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_cmu_us_rms_cg.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_sltRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_cmu_us_slt_cg.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::isSelectedVoiceInstalled(QString voice)
{
    bool found = false;
    int size = engineInfo->installedVoices.size();
    for (int i = 0; i < size; i++)
    {
        VoiceInfo tmp = engineInfo->installedVoices.at(i);
        if (tmp.name == voice)
            found = true;
    }
    if (!found)
    {
        emit updateVoice();
    }
}

void InstallVoicesDialog::on_aewFliteInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_aew_flite.sh";
    downloadFilename = "/tmp/cmu_us_aew.flitevox";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/flite/2.1/cmu_us_aew.flitevox";
    startDownload();
}

void InstallVoicesDialog::on_ahwFliteInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_ahw_flite.sh";
    downloadFilename = "/tmp/cmu_us_ahw.flitevox";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/flite/2.1/cmu_us_ahw.flitevox";
    startDownload();
}

void InstallVoicesDialog::on_aupFliteInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_aup_flite.sh";
    downloadFilename = "/tmp/cmu_us_aup.flitevox";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/flite/2.1/cmu_us_aup.flitevox";
    startDownload();
}

void InstallVoicesDialog::on_awb2FliteInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_awb_flite.sh";
    downloadFilename = "/tmp/cmu_us_awb.flitevox";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/flite/2.1/cmu_us_awb.flitevox";
    startDownload();
}

void InstallVoicesDialog::on_axbFliteInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_axb_flite.sh";
    downloadFilename = "/tmp/cmu_us_axb.flitevox";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/flite/2.1/cmu_us_axb.flitevox";
    startDownload();
}

void InstallVoicesDialog::on_bdlFliteInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_bdl_flite.sh";
    downloadFilename = "/tmp/cmu_us_bdl.flitevox";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/flite/2.1/cmu_us_bdl.flitevox";
    startDownload();
}

void InstallVoicesDialog::on_clbFliteInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_clb_flite.sh";
    downloadFilename = "/tmp/cmu_us_clb.flitevox";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/flite/2.1/cmu_us_clb.flitevox";
    startDownload();
}

void InstallVoicesDialog::on_eeyFliteInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_eey_flite.sh";
    downloadFilename = "/tmp/cmu_us_eey.flitevox";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/flite/2.1/cmu_us_eey.flitevox";
    startDownload();
}

void InstallVoicesDialog::on_femFliteInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_fem_flite.sh";
    downloadFilename = "/tmp/cmu_us_fem.flitevox";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/flite/2.1/cmu_us_fem.flitevox";
    startDownload();
}

void InstallVoicesDialog::on_gkaFliteInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_gka_flite.sh";
    downloadFilename = "/tmp/cmu_us_gka.flitevox";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/flite/2.1/cmu_us_gka.flitevox";
    startDownload();
}

void InstallVoicesDialog::on_jmkFliteInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_jmk_flite.sh";
    downloadFilename = "/tmp/cmu_us_jmk.flitevox";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/flite/2.1/cmu_us_jmk.flitevox";
    startDownload();
}

void InstallVoicesDialog::on_kspFliteInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_ksp_flite.sh";
    downloadFilename = "/tmp/cmu_us_ksp.flitevox";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/flite/2.1/cmu_us_ksp.flitevox";
    startDownload();
}

void InstallVoicesDialog::on_ljmFliteInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_ljm_flite.sh";
    downloadFilename = "/tmp/cmu_us_ljm.flitevox";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/flite/2.1/cmu_us_ljm.flitevox";
    startDownload();
}

void InstallVoicesDialog::on_lnhFliteInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_lnh_flite.sh";
    downloadFilename = "/tmp/cmu_us_lnh.flitevox";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/flite/2.1/cmu_us_lnh.flitevox";
    startDownload();
}

void InstallVoicesDialog::on_rms2FliteInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_rms_flite.sh";
    downloadFilename = "/tmp/cmu_us_rms.flitevox";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/flite/2.1/cmu_us_rms.flitevox";
    startDownload();
}

void InstallVoicesDialog::on_rxrFliteInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_rxr_flite.sh";
    downloadFilename = "/tmp/cmu_us_rxr.flitevox";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/flite/2.1/cmu_us_rxr.flitevox";
    startDownload();
}

void InstallVoicesDialog::on_slpFliteInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_slp_flite.sh";
    downloadFilename = "/tmp/cmu_us_slp.flitevox";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/flite/2.1/cmu_us_slp.flitevox";
    startDownload();
}

void InstallVoicesDialog::on_slt2FliteInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_slt_flite.sh";
    downloadFilename = "/tmp/cmu_us_slt.flitevox";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/flite/2.1/cmu_us_slt.flitevox";
    startDownload();
}

void InstallVoicesDialog::on_indicBenRmFliteInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_indic_ben_rm_flite.sh";
    downloadFilename = "/tmp/cmu_indic_ben_rm.flitevox";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/flite/2.1/cmu_indic_ben_rm.flitevox";
    startDownload();
}

void InstallVoicesDialog::on_IndicGujAdFliteInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_indic_guj_ad_flite.sh";
    downloadFilename = "/tmp/cmu_indic_guj_ad.flitevox";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/flite/2.1/cmu_indic_guj_ad.flitevox";
    startDownload();
}

void InstallVoicesDialog::on_IndicGujDpFliteInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_indic_guj_dp_flite.sh";
    downloadFilename = "/tmp/cmu_indic_guj_dp.flitevox";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/flite/2.1/cmu_indic_guj_dp.flitevox";    startDownload();}

void InstallVoicesDialog::on_IndicGujKtFliteInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_indic_guj_kt_flite.sh";
    downloadFilename = "/tmp/cmu_indic_guj_kt.flitevox";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/flite/2.1/cmu_indic_guj_kt.flitevox";
    startDownload();
}

void InstallVoicesDialog::on_IndicHinAbFliteInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_indic_hin_ab_flite.sh";
    downloadFilename = "/tmp/cmu_indic_hin_ab.flitevox";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/flite/2.1/cmu_indic_hin_ab.flitevox";
    startDownload();
}

void InstallVoicesDialog::on_IndicKanPlvFliteInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_indic_kan_plv_flite.sh";
    downloadFilename = "/tmp/cmu_indic_kan_plv.flitevox";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/flite/2.1/cmu_indic_kan_plv.flitevox";
    startDownload();
}

void InstallVoicesDialog::on_IndicMarAupFliteInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_indic_mar_aup_flite.sh";
    downloadFilename = "/tmp/cmu_indic_mar_aup.flitevox";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/flite/2.1/cmu_indic_mar_aup.flitevox";
    startDownload();
}

void InstallVoicesDialog::on_IndicMarSlpFliteInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_indic_mar_slp_flite.sh";
    downloadFilename = "/tmp/cmu_indic_mar_slp.flitevox";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/flite/2.1/cmu_indic_mar_slp.flitevox";
    startDownload();
}

void InstallVoicesDialog::on_IndicPanAmpFliteInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_indic_pan_amp_flite.sh";
    downloadFilename = "/tmp/cmu_indic_pan_amp.flitevox";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/flite/2.1/cmu_indic_pan_amp.flitevox";
    startDownload();
}


void InstallVoicesDialog::on_IndicTamSdrFliteInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_indic_tam_sdr_flite.sh";
    downloadFilename = "/tmp/cmu_indic_tam_sdr.flitevox";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/flite/2.1/cmu_indic_tam_sdr.flitevox";
    startDownload();
}


void InstallVoicesDialog::on_IndicTelKpnFliteInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_indic_tel_kpn_flite.sh";
    downloadFilename = "/tmp/cmu_indic_tel_kpn.flitevox";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/flite/2.1/cmu_indic_tel_kpn.flitevox";
    startDownload();
}

void InstallVoicesDialog::on_IndicTelSkFliteInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_indic_tel_sk_flite.sh";
    downloadFilename = "/tmp/cmu_indic_tel_sk.flitevox";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/flite/2.1/cmu_indic_tel_sk.flitevox";
    startDownload();
}

void InstallVoicesDialog::on_IndicTelSsFliteInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_indic_tel_ss_flite.sh";
    downloadFilename = "/tmp/cmu_indic_tel_ss.flitevox";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/flite/2.1/cmu_indic_tel_ss.flitevox";
    startDownload();
}

void InstallVoicesDialog::on_aewFliteRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_aew_flite.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_ahwFliteRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_ahw_flite.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_aupFliteRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_aup_flite.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_awb2FliteRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_awb_flite.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_axbFliteRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_axb_flite.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_bdlFliteRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_bdl_flite.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_clbFliteRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_clb_flite.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_eeyFliteRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_eey_flite.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_femFliteRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_fem_flite.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_gkaFliteRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_gka_flite.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_jmkFliteRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_jmk_flite.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_kspFliteRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_ksp_flite.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_ljmFliteRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_ljm_flite.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_lnhFliteRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_lnh_flite.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_rms2FliteRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_rms_flite.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_rxrFliteRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_rxr_flite.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_slpFliteRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_slp_flite.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_slt2FliteRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_slt_flite.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_indicBenRmFliteRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_indic_ben_rm_flite.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_IndicGujAdFliteRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_indic_guj_ad_flite.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_IndicGujDpFliteRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_indic_guj_dp_flite.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_IndicGujKtFliteRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_indic_guj_kt_flite.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_IndicHinAbFliteRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_indic_hin_ab_flite.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_IndicKanPlvFliteRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_indic_kan_plv_flite.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_IndicMarAupFliteRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_indic_mar_aup_flite.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_IndicMarSlpFliteRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_indic_mar_slp_flite.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}
void InstallVoicesDialog::on_IndicPanAmpFliteRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_indic_pan_amp_flite.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_IndicTamSdrFliteRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_indic_tam_sdr_flite.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}


void InstallVoicesDialog::on_IndicTelKpnFliteRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_indic_tel_kpn_flite.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_IndicTelSkFliteRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_indic_tel_sk_flite.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_IndicTelSsFliteRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_indic_tel_ss_flite.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_femInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_cmu_us_fem_cg.sh";
    downloadFilename = "/tmp/festvox_cmu_us_fem_cg.tar.gz";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/festvox_cmu_us_fem_cg.tar.gz";
    startDownload();
}


void InstallVoicesDialog::on_femRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_cmu_us_fem_cg.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}


void InstallVoicesDialog::on_gkaInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_cmu_us_gka_cg.sh";
    downloadFilename = "/tmp/festvox_cmu_us_gka_cg.tar.gz";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/festvox_cmu_us_gka_cg.tar.gz";
    startDownload();
}


void InstallVoicesDialog::on_gkaRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_cmu_us_gka_cg.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}


void InstallVoicesDialog::on_ljmInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_cmu_us_ljm_cg.sh";
    downloadFilename = "/tmp/festvox_cmu_us_ljm_cg.tar.gz";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/festvox_cmu_us_ljm_cg.tar.gz";
    startDownload();
}

void InstallVoicesDialog::on_lnhInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_cmu_us_lnh_cg.sh";
    downloadFilename = "/tmp/festvox_cmu_us_lnh_cg.tar.gz";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/festvox_cmu_us_lnh_cg.tar.gz";
    startDownload();
}

void InstallVoicesDialog::on_rxrInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_cmu_us_rxr_cg.sh";
    downloadFilename = "/tmp/festvox_cmu_us_rxr_cg.tar.gz";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/festvox_cmu_us_rxr_cg.tar.gz";
    startDownload();
}

void InstallVoicesDialog::on_slpInstallButton_clicked()
{
    installationCommandPart2 = " /usr/share/omilo-qt5/scripts/install_cmu_us_slp_cg.sh";
    downloadFilename = "/tmp/festvox_cmu_us_slp_cg.tar.gz";
    downloadCommand = "cd /tmp && wget -c http://sourceforge.net/projects/o-milo/files/Omilo/linux/voices/festvox_cmu_us_slp_cg.tar.gz";
    startDownload();
}


void InstallVoicesDialog::on_ljmRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_cmu_us_ljm_cg.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_lnhRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_cmu_us_lnh_cg.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_rxrRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_cmu_us_rxr_cg.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}

void InstallVoicesDialog::on_slpRemoveButton_clicked()
{
    removeCommandPart2 = " /usr/share/omilo-qt5/scripts/remove_cmu_us_slp_cg.sh";
    removeProcess->start("sh", QStringList() << "-c" << sudoCommand + removeCommandPart2);
}


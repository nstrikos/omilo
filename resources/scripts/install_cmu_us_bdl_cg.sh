#! /bin/bash
tar -xzf /tmp/festvox_cmu_us_bdl_cg.tar.gz -C /tmp
mkdir -p /usr/share/omilo-qt5/festival/lib/voices/us
mv /tmp/festival/lib/voices/us/cmu_us_bdl_cg /usr/share/omilo-qt5/festival/lib/voices/us
rm -rf /tmp/festival/
rm /tmp/festvox_cmu_us_bdl_cg.tar.gz

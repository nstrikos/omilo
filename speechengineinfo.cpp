#include "speechengineinfo.h"

SpeechEngineInfo::SpeechEngineInfo()
{
    qDebug() << "Creating new information class...";
    update();
    qDebug() << "Creating new information class completed.";
}

SpeechEngineInfo::~SpeechEngineInfo()
{
    qDebug() << "Deleting information class...";
}

//If we need to add a new voice we need to re-code this function
void SpeechEngineInfo::update()
{
    qDebug() << "Updating speech engine info...";
    installedVoices.clear();
    availableVoices.clear();
    VoiceInfo voiceToAdd;

    voiceToAdd.mode = piper;
    voiceToAdd.name = LessacLowPiper;
    voiceToAdd.language = QLocale::languageToString(QLocale::English);
    installedVoices.push_back(voiceToAdd);

    voiceToAdd.mode = festival;
    voiceToAdd.name = KalFestival;
    voiceToAdd.language = QLocale::languageToString(QLocale::English);
    if(QDir("/usr/share/omilo-qt5/festival/lib/voices/english/kal_diphone").exists())
        installedVoices.push_back(voiceToAdd);
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.mode = festivalCmu;
    voiceToAdd.name = AwbCmuFestival;
    voiceToAdd.language = QLocale::languageToString(QLocale::English);
    if(QDir("/usr/share/omilo-qt5/festival/lib/voices/us/cmu_us_awb_cg").exists())
        installedVoices.push_back(voiceToAdd);
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = AewCmuFestival;
    voiceToAdd.language = QLocale::languageToString(QLocale::English);
    if(QDir("/usr/share/omilo-qt5/festival/lib/voices/us/cmu_us_aew_cg").exists())
        installedVoices.push_back(voiceToAdd);
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = AhwCmuFestival;
    voiceToAdd.language = QLocale::languageToString(QLocale::English);
    if(QDir("/usr/share/omilo-qt5/festival/lib/voices/us/cmu_us_ahw_cg").exists())
        installedVoices.push_back(voiceToAdd);
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = AupCmuFestival;
    voiceToAdd.language = QLocale::languageToString(QLocale::English);
    if(QDir("/usr/share/omilo-qt5/festival/lib/voices/us/cmu_us_aup_cg").exists())
        installedVoices.push_back(voiceToAdd);
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = AxbCmuFestival;
    voiceToAdd.language = QLocale::languageToString(QLocale::English);
    if(QDir("/usr/share/omilo-qt5/festival/lib/voices/us/cmu_us_axb_cg").exists())
        installedVoices.push_back(voiceToAdd);
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = BdlCmuFestival;
    voiceToAdd.language = QLocale::languageToString(QLocale::English);
    if(QDir("/usr/share/omilo-qt5/festival/lib/voices/us/cmu_us_bdl_cg").exists())
        installedVoices.push_back(voiceToAdd);
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = ClbCmuFestival;
    voiceToAdd.language = QLocale::languageToString(QLocale::English);
    if(QDir("/usr/share/omilo-qt5/festival/lib/voices/us/cmu_us_clb_cg").exists())
        installedVoices.push_back(voiceToAdd);
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = EeyCmuFestival;
    voiceToAdd.language = QLocale::languageToString(QLocale::English);
    if(QDir("/usr/share/omilo-qt5/festival/lib/voices/us/cmu_us_eey_cg").exists())
        installedVoices.push_back(voiceToAdd);
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = FemCmuFestival;
    voiceToAdd.language = QLocale::languageToString(QLocale::English);
    if(QDir("/usr/share/omilo-qt5/festival/lib/voices/us/cmu_us_fem_cg").exists())
        installedVoices.push_back(voiceToAdd);
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = GkaCmuFestival;
    voiceToAdd.language = QLocale::languageToString(QLocale::English);
    if(QDir("/usr/share/omilo-qt5/festival/lib/voices/us/cmu_us_gka_cg").exists())
        installedVoices.push_back(voiceToAdd);
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = LjmCmuFestival;
    voiceToAdd.language = QLocale::languageToString(QLocale::English);
    if(QDir("/usr/share/omilo-qt5/festival/lib/voices/us/cmu_us_ljm_cg").exists())
        installedVoices.push_back(voiceToAdd);
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = LnhCmuFestival;
    voiceToAdd.language = QLocale::languageToString(QLocale::English);
    if(QDir("/usr/share/omilo-qt5/festival/lib/voices/us/cmu_us_lnh_cg").exists())
        installedVoices.push_back(voiceToAdd);
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = JmkCmuFestival;
    voiceToAdd.language = QLocale::languageToString(QLocale::English);
    if(QDir("/usr/share/omilo-qt5/festival/lib/voices/us/cmu_us_jmk_cg").exists())
        installedVoices.push_back(voiceToAdd);
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = KspCmuFestival;
    voiceToAdd.language = QLocale::languageToString(QLocale::English);
    if(QDir("/usr/share/omilo-qt5/festival/lib/voices/us/cmu_us_ksp_cg").exists())
        installedVoices.push_back(voiceToAdd);
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = RmsCmuFestival;
    voiceToAdd.language = QLocale::languageToString(QLocale::English);
    if(QDir("/usr/share/omilo-qt5/festival/lib/voices/us/cmu_us_rms_cg").exists())
        installedVoices.push_back(voiceToAdd);
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = RxrCmuFestival;
    voiceToAdd.language = QLocale::languageToString(QLocale::English);
    if(QDir("/usr/share/omilo-qt5/festival/lib/voices/us/cmu_us_rxr_cg").exists())
        installedVoices.push_back(voiceToAdd);
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = SlpCmuFestival;
    voiceToAdd.language = QLocale::languageToString(QLocale::English);
    if(QDir("/usr/share/omilo-qt5/festival/lib/voices/us/cmu_us_slp_cg").exists())
        installedVoices.push_back(voiceToAdd);
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = SltCmuFestival;
    voiceToAdd.language = QLocale::languageToString(QLocale::English);
    if(QDir("/usr/share/omilo-qt5/festival/lib/voices/us/cmu_us_slt_cg").exists())
        installedVoices.push_back(voiceToAdd);
    else
        availableVoices.push_back(voiceToAdd);

 #ifndef Q_OS_WIN
    voiceToAdd.name = CustomFestival;
    installedVoices.push_back(voiceToAdd);
#endif

    voiceToAdd.mode = flite;
    voiceToAdd.language = QLocale::languageToString(QLocale::English);
    if (QFile::exists("/usr/share/omilo-qt5/flite/flite") ||
        QFile::exists(QCoreApplication::applicationDirPath() + "\\flite.exe") )
    {
        voiceToAdd.name = RmsFlite;
        installedVoices.push_back(voiceToAdd);
        voiceToAdd.name = SltFlite;
        installedVoices.push_back(voiceToAdd);
        voiceToAdd.name = AwbFlite;
        installedVoices.push_back(voiceToAdd);
        voiceToAdd.name = Kal16Flite;
        installedVoices.push_back(voiceToAdd);
    }    

    voiceToAdd.name = AewFlite;
    if (QFile::exists("/usr/share/omilo-qt5/flite/voices/cmu_us_aew.flitevox"))
    {
        installedVoices.push_back(voiceToAdd);
    }
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = AhwFlite;
    if (QFile::exists("/usr/share/omilo-qt5/flite/voices/cmu_us_ahw.flitevox"))
    {
        installedVoices.push_back(voiceToAdd);
    }
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = AupFlite;
    if (QFile::exists("/usr/share/omilo-qt5/flite/voices/cmu_us_aup.flitevox"))
    {
        installedVoices.push_back(voiceToAdd);
    }
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = Awb2Flite;
    if (QFile::exists("/usr/share/omilo-qt5/flite/voices/cmu_us_awb.flitevox"))
    {
        installedVoices.push_back(voiceToAdd);
    }
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = AxbFlite;
    if (QFile::exists("/usr/share/omilo-qt5/flite/voices/cmu_us_axb.flitevox"))
    {
        installedVoices.push_back(voiceToAdd);
    }
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = BdlFlite;
    if (QFile::exists("/usr/share/omilo-qt5/flite/voices/cmu_us_bdl.flitevox"))
    {
        installedVoices.push_back(voiceToAdd);
    }
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = ClbFlite;
    if (QFile::exists("/usr/share/omilo-qt5/flite/voices/cmu_us_clb.flitevox"))
    {
        installedVoices.push_back(voiceToAdd);
    }
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = EeyFlite;
    if (QFile::exists("/usr/share/omilo-qt5/flite/voices/cmu_us_eey.flitevox"))
    {
        installedVoices.push_back(voiceToAdd);
    }
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = FemFlite;
    if (QFile::exists("/usr/share/omilo-qt5/flite/voices/cmu_us_fem.flitevox"))
    {
        installedVoices.push_back(voiceToAdd);
    }
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = GkaFlite;
    if (QFile::exists("/usr/share/omilo-qt5/flite/voices/cmu_us_gka.flitevox"))
    {
        installedVoices.push_back(voiceToAdd);
    }
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = JmkFlite;
    if (QFile::exists("/usr/share/omilo-qt5/flite/voices/cmu_us_jmk.flitevox"))
    {
        installedVoices.push_back(voiceToAdd);
    }
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = KspFlite;
    if (QFile::exists("/usr/share/omilo-qt5/flite/voices/cmu_us_ksp.flitevox"))
    {
        installedVoices.push_back(voiceToAdd);
    }
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = LjmFlite;
    if (QFile::exists("/usr/share/omilo-qt5/flite/voices/cmu_us_ljm.flitevox"))
    {
        installedVoices.push_back(voiceToAdd);
    }
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = LnhFlite;
    if (QFile::exists("/usr/share/omilo-qt5/flite/voices/cmu_us_lnh.flitevox"))
    {
        installedVoices.push_back(voiceToAdd);
    }
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = Rms2Flite;
    if (QFile::exists("/usr/share/omilo-qt5/flite/voices/cmu_us_rms.flitevox"))
    {
        installedVoices.push_back(voiceToAdd);
    }
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = RxrFlite;
    if (QFile::exists("/usr/share/omilo-qt5/flite/voices/cmu_us_rxr.flitevox"))
    {
        installedVoices.push_back(voiceToAdd);
    }
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = SlpFlite;
    if (QFile::exists("/usr/share/omilo-qt5/flite/voices/cmu_us_slp.flitevox"))
    {
        installedVoices.push_back(voiceToAdd);
    }
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = Slt2Flite;
    if (QFile::exists("/usr/share/omilo-qt5/flite/voices/cmu_us_slt.flitevox"))
    {
        installedVoices.push_back(voiceToAdd);
    }
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = IndicBenRmFlite;
    if (QFile::exists("/usr/share/omilo-qt5/flite/voices/cmu_indic_ben_rm.flitevox"))
    {
        installedVoices.push_back(voiceToAdd);
    }
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = IndicGujAdFlite;
    if (QFile::exists("/usr/share/omilo-qt5/flite/voices/cmu_indic_guj_ad.flitevox"))
    {
        installedVoices.push_back(voiceToAdd);
    }
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = IndicGujDpFlite;
    if (QFile::exists("/usr/share/omilo-qt5/flite/voices/cmu_indic_guj_dp.flitevox"))
    {
        installedVoices.push_back(voiceToAdd);
    }
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = IndicGujKtFlite;
    if (QFile::exists("/usr/share/omilo-qt5/flite/voices/cmu_indic_guj_kt.flitevox"))
    {
        installedVoices.push_back(voiceToAdd);
    }
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = IndicHinAbFlite;
    if (QFile::exists("/usr/share/omilo-qt5/flite/voices/cmu_indic_hin_ab.flitevox"))
    {
        installedVoices.push_back(voiceToAdd);
    }
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = IndicKanPlvFlite;
    if (QFile::exists("/usr/share/omilo-qt5/flite/voices/cmu_indic_kan_plv.flitevox"))
    {
        installedVoices.push_back(voiceToAdd);
    }
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = IndicMarAupFlite;
    if (QFile::exists("/usr/share/omilo-qt5/flite/voices/cmu_indic_mar_aup.flitevox"))
    {
        installedVoices.push_back(voiceToAdd);
    }
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = IndicMarSlpFlite;
    if (QFile::exists("/usr/share/omilo-qt5/flite/voices/cmu_indic_mar_slp.flitevox"))
    {
        installedVoices.push_back(voiceToAdd);
    }
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = IndicPanAmpFlite;
    if (QFile::exists("/usr/share/omilo-qt5/flite/voices/cmu_indic_pan_amp.flitevox"))
    {
        installedVoices.push_back(voiceToAdd);
    }
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = IndicTamSdrFlite;
    if (QFile::exists("/usr/share/omilo-qt5/flite/voices/cmu_indic_tam_sdr.flitevox"))
    {
        installedVoices.push_back(voiceToAdd);
    }
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = IndicTelKpnFlite;
    if (QFile::exists("/usr/share/omilo-qt5/flite/voices/cmu_indic_tel_kpn.flitevox"))
    {
        installedVoices.push_back(voiceToAdd);
    }
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = IndicTelSkFlite;
    if (QFile::exists("/usr/share/omilo-qt5/flite/voices/cmu_indic_tel_sk.flitevox"))
    {
        installedVoices.push_back(voiceToAdd);
    }
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.name = IndicTelSsFlite;
    if (QFile::exists("/usr/share/omilo-qt5/flite/voices/cmu_indic_tel_ss.flitevox"))
    {
        installedVoices.push_back(voiceToAdd);
    }
    else
        availableVoices.push_back(voiceToAdd);

    voiceToAdd.mode = marytts;
    if (QDir(maryInstallationDir).exists())
    {
        voiceToAdd.name = ItalianMary;
        voiceToAdd.language = QLocale::languageToString(QLocale::Italian);
        installedVoices.push_back(voiceToAdd);
        voiceToAdd.name = GermanMary;
        voiceToAdd.language = QLocale::languageToString(QLocale::German);
        installedVoices.push_back(voiceToAdd);
        voiceToAdd.name = TurkishMary;
        voiceToAdd.language = QLocale::languageToString(QLocale::Turkish);
        installedVoices.push_back(voiceToAdd);
        voiceToAdd.name = GreekMary;
        voiceToAdd.language = QLocale::languageToString(QLocale::Greek);
        installedVoices.push_back(voiceToAdd);
        voiceToAdd.name = GreekHercules;
        voiceToAdd.language = QLocale::languageToString(QLocale::Greek);
        installedVoices.push_back(voiceToAdd);
        voiceToAdd.name = GreekGoogleMary;
        voiceToAdd.language = QLocale::languageToString(QLocale::Greek);
        installedVoices.push_back(voiceToAdd);
        voiceToAdd.name = FrenchMary;
        voiceToAdd.language = QLocale::languageToString(QLocale::French);
        installedVoices.push_back(voiceToAdd);
        voiceToAdd.language = QLocale::languageToString(QLocale::English);
        voiceToAdd.name = CmuBdlMary;
        installedVoices.push_back(voiceToAdd);
        voiceToAdd.language = QLocale::languageToString(QLocale::English);
        voiceToAdd.name = CmuRmsMary;
        installedVoices.push_back(voiceToAdd);
        voiceToAdd.name = CmuSltMary;
        installedVoices.push_back(voiceToAdd);
        voiceToAdd.name = ObadiahMary;
        installedVoices.push_back(voiceToAdd);
        voiceToAdd.name = PoppyMary;
        installedVoices.push_back(voiceToAdd);
        voiceToAdd.name = PrudenceMary;
        installedVoices.push_back(voiceToAdd);
        voiceToAdd.name = SpikeMary;
        installedVoices.push_back(voiceToAdd);
    }
}

engineMode SpeechEngineInfo::getVoiceMode(QString voice)
{
    unsigned int i;
    QString voiceString;

    if (installedVoices.size() > 0)
    {
        for (i = 0; i < installedVoices.size() - 1; i++)
        {
            voiceString = installedVoices.at(i).name;
            if (voiceString == voice)
            {
                return installedVoices.at(i).mode;
            }
        }
    }
    return unknown;
}

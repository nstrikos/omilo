#ifndef INSTALLVOICESDIALOG_H
#define INSTALLVOICESDIALOG_H

#include <QDialog>
#include <QProcess>
#include "ui_installvoicesdialog.h"
#include "progressdialog.h"
#include "speechengineinfo.h"

class InstallVoicesDialog : public QDialog, public Ui::InstallVoicesDialog
{
    Q_OBJECT
    
public:
    InstallVoicesDialog(QWidget *parent = 0);
    ~InstallVoicesDialog();
    void showWindow(QString currentVoice);

public slots:
    void isSelectedVoiceInstalled(QString voice);

private slots:
    void downloadFinished();
    void installationFinished();
    void removeFinished();
    void on_closeButton_clicked();
    void on_awbInstallButton_clicked();
    void on_aewInstallButton_clicked();
    void on_bdlInstallButton_clicked();
    void on_clbInstallButton_clicked();
    void on_jmkInstallButton_clicked();
    void on_kspInstallButton_clicked();
    void on_rmsInstallButton_clicked();
    void on_sltInstallButton_clicked();
    void on_awbRemoveButton_clicked();
    void on_aewRemoveButton_clicked();
    void on_bdlRemoveButton_clicked();
    void on_clbRemoveButton_clicked();
    void on_jmkRemoveButton_clicked();
    void on_kspRemoveButton_clicked();
    void on_rmsRemoveButton_clicked();
    void on_sltRemoveButton_clicked();

    void on_aewFliteInstallButton_clicked();

    void on_ahwFliteInstallButton_clicked();

    void on_aupFliteInstallButton_clicked();

    void on_awb2FliteInstallButton_clicked();

    void on_axbFliteInstallButton_clicked();

    void on_bdlFliteInstallButton_clicked();

    void on_clbFliteInstallButton_clicked();

    void on_eeyFliteInstallButton_clicked();

    void on_femFliteInstallButton_clicked();

    void on_gkaFliteInstallButton_clicked();

    void on_jmkFliteInstallButton_clicked();

    void on_kspFliteInstallButton_clicked();

    void on_ljmFliteInstallButton_clicked();

    void on_lnhFliteInstallButton_clicked();

    void on_rms2FliteInstallButton_clicked();

    void on_rxrFliteInstallButton_clicked();

    void on_slpFliteInstallButton_clicked();

    void on_slt2FliteInstallButton_clicked();

    void on_indicBenRmFliteInstallButton_clicked();

    void on_IndicGujAdFliteInstallButton_clicked();

    void on_IndicGujDpFliteInstallButton_clicked();

    void on_IndicGujKtFliteInstallButton_clicked();

    void on_IndicHinAbFliteInstallButton_clicked();

    void on_IndicKanPlvFliteInstallButton_clicked();

    void on_IndicMarAupFliteInstallButton_clicked();

    void on_IndicMarSlpFliteInstallButton_clicked();

    void on_IndicPanAmpFliteInstallButton_clicked();

    void on_IndicTamSdrFliteInstallButton_clicked();

    void on_IndicTelKpnFliteInstallButton_clicked();

    void on_IndicTelSsFliteInstallButton_clicked();

    void on_IndicTelSkFliteInstallButton_clicked();

    void on_aewFliteRemoveButton_clicked();

    void on_ahwFliteRemoveButton_clicked();

    void on_aupFliteRemoveButton_clicked();

    void on_awb2FliteRemoveButton_clicked();

    void on_axbFliteRemoveButton_clicked();

    void on_bdlFliteRemoveButton_clicked();

    void on_clbFliteRemoveButton_clicked();

    void on_eeyFliteRemoveButton_clicked();

    void on_femFliteRemoveButton_clicked();

    void on_gkaFliteRemoveButton_clicked();

    void on_jmkFliteRemoveButton_clicked();

    void on_kspFliteRemoveButton_clicked();

    void on_ljmFliteRemoveButton_clicked();

    void on_lnhFliteRemoveButton_clicked();

    void on_rms2FliteRemoveButton_clicked();

    void on_rxrFliteRemoveButton_clicked();

    void on_slpFliteRemoveButton_clicked();

    void on_slt2FliteRemoveButton_clicked();

    void on_indicBenRmFliteRemoveButton_clicked();

    void on_IndicGujAdFliteRemoveButton_clicked();

    void on_IndicGujDpFliteRemoveButton_clicked();

    void on_IndicGujKtFliteRemoveButton_clicked();

    void on_IndicHinAbFliteRemoveButton_clicked();

    void on_IndicKanPlvFliteRemoveButton_clicked();

    void on_IndicMarAupFliteRemoveButton_clicked();

    void on_IndicMarSlpFliteRemoveButton_clicked();

    void on_IndicPanAmpFliteRemoveButton_clicked();

    void on_IndicTamSdrFliteRemoveButton_clicked();

    void on_IndicTelKpnFliteRemoveButton_clicked();

    void on_IndicTelSkFliteRemoveButton_clicked();

    void on_IndicTelSsFliteRemoveButton_clicked();

    void on_ahwInstallButton_clicked();

    void on_ahwRemoveButton_clicked();

    void on_aupInstallButton_clicked();

    void on_aupRemoveButton_clicked();

    void on_axbInstallButton_clicked();

    void on_axbRemoveButton_clicked();

    void on_eeyInstallButton_clicked();

    void on_eeyRemoveButton_clicked();

    void on_femInstallButton_clicked();

    void on_femRemoveButton_clicked();

    void on_gkaInstallButton_clicked();

    void on_gkaRemoveButton_clicked();

    void on_ljmInstallButton_clicked();

    void on_ljmRemoveButton_clicked();

    void on_lnhInstallButton_clicked();

    void on_lnhRemoveButton_clicked();

    void on_rxrInstallButton_clicked();

    void on_rxrRemoveButton_clicked();

    void on_slpInstallButton_clicked();

    void on_slpRemoveButton_clicked();

private:
    void updateAvailableVoices();
    SpeechEngineInfo *engineInfo;
    QString downloadCommand;
    QString installationCommandPart2;
    QString sudoCommand;
    QString removeCommandPart2;
    QProcess *downloadProcess;
    QProcess *installationProcess;
    QProcess *removeProcess;
    progressDialog *myProgressDialog;
    QString downloadFilename;
    void startDownload();
    QString currentVoice;

signals:
    void updateVoice();
};

#endif // INSTALLVOICESDIALOG_H

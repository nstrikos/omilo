#include "progressdialog.h"
#include "ui_progressdialog.h"

progressDialog::progressDialog(QProcess *process, QString filename, QWidget *parent) :QDialog(parent)
{
    setupUi(this);
    setWindowFlags(Qt::Dialog
                   | Qt::FramelessWindowHint
                   | Qt::CustomizeWindowHint);
    this->downloadProcess = process;
    this->downloadFile = filename;
    progressBar->setValue(0);
    updateMaxSize();
    connect(&timer, SIGNAL(timeout()), this, SLOT(checkDownloadFileSize()));
    timer.start(1000);
}

void progressDialog::closeEvent(QCloseEvent *event)
{
    event->ignore();
}

void progressDialog::updateProgressDialog(QProcess *process, QString filename)
{
    this->downloadProcess = process;
    this->downloadFile = filename;
    this->progressBar->setValue(0);
    timer.start(1000);
    updateMaxSize();
}

void progressDialog::on_cancelButton_clicked()
{
    QFile::remove(this->downloadFile);
    downloadProcess->close();
    timer.stop();
}

void progressDialog::updateMaxSize()
{
    if (downloadFile == "/tmp/festvox_cmu_us_awb_cg.tar.gz")
        maxSize = 52;
    else if (downloadFile == "/tmp/festvox_cmu_us_aew_cg.tar.gz")
        maxSize = 61;
    else if (downloadFile == "/tmp/festvox_cmu_us_ahw_cg.tar.gz")
        maxSize = 31;
    else if (downloadFile == "/tmp/festvox_cmu_us_aup_cg.tar.gz")
        maxSize = 25;
    else if (downloadFile == "/tmp/festvox_cmu_us_axb_cg.tar.gz")
        maxSize = 33;
    else if (downloadFile == "/tmp/festvox_cmu_us_bdl_cg.tar.gz")
        maxSize = 48;
    else if (downloadFile == "/tmp/festvox_cmu_us_clb_cg.tar.gz")
        maxSize = 60;
    else if (downloadFile == "/tmp/festvox_cmu_us_eey_cg.tar.gz")
        maxSize = 27;
    else if (downloadFile == "/tmp/festvox_cmu_us_fem_cg.tar.gz")
        maxSize = 27;
    else if (downloadFile == "/tmp/festvox_cmu_us_gka_cg.tar.gz")
        maxSize = 31;
    else if (downloadFile == "/tmp/festvox_cmu_us_jmk_cg.tar.gz")
        maxSize = 49;
    else if (downloadFile == "/tmp/festvox_cmu_us_ksp_cg.tar.gz")
        maxSize = 56;
    else if (downloadFile == "/tmp/festvox_cmu_us_ljm_cg.tar.gz")
        maxSize = 25;
    else if (downloadFile == "/tmp/festvox_cmu_us_lnh_cg.tar.gz")
        maxSize = 57;
    else if (downloadFile == "/tmp/festvox_cmu_us_rms_cg.tar.gz")
        maxSize = 61;
    else if (downloadFile == "/tmp/festvox_cmu_us_rxr_cg.tar.gz")
        maxSize = 36;
    else if (downloadFile == "/tmp/festvox_cmu_us_slp_cg.tar.gz")
        maxSize = 33;
    else if (downloadFile == "/tmp/festvox_cmu_us_slt_cg.tar.gz")
        maxSize = 53;
    else if (downloadFile == "/tmp/cmu_us_aew.flitevox")
        maxSize = 12;
    else if (downloadFile == "/tmp/cmu_us_ahw.flitevox")
        maxSize = 7;
    else if (downloadFile == "/tmp/cmu_us_aup.flitevox")
        maxSize = 5;
    else if (downloadFile == "/tmp/cmu_us_awb.flitevox")
        maxSize = 11;
    else if (downloadFile == "/tmp/cmu_us_axb.flitevox")
        maxSize = 7;
    else if (downloadFile == "/tmp/cmu_us_bdl.flitevox")
        maxSize = 10;
    else if (downloadFile == "/tmp/cmu_us_clb.flitevox")
        maxSize = 12;
    else if (downloadFile == "/tmp/cmu_us_eey.flitevox")
        maxSize = 7;
    else if (downloadFile == "/tmp/cmu_us_fem.flitevox")
        maxSize = 7;
    else if (downloadFile == "/tmp/cmu_us_gka.flitevox")
        maxSize = 7;
    else if (downloadFile == "/tmp/cmu_us_jmk.flitevox")
        maxSize = 11;
    else if (downloadFile == "/tmp/cmu_us_ksp.flitevox")
        maxSize = 11;
    else if (downloadFile == "/tmp/cmu_us_ljm.flitevox")
        maxSize = 5;
    else if (downloadFile == "/tmp/cmu_us_lnh.flitevox")
        maxSize = 11;
    else if (downloadFile == "/tmp/cmu_us_rms.flitevox")
        maxSize = 11;
    else if (downloadFile == "/tmp/cmu_us_rxr.flitevox")
        maxSize = 7;
    else if (downloadFile == "/tmp/cmu_us_slp.flitevox")
        maxSize = 7;
    else if (downloadFile == "/tmp/cmu_us_slt.flitevox")
        maxSize = 11;
    else if (downloadFile == "/tmp/cmu_indic_ben_rm.flitevox")
        maxSize = 9;
    else if (downloadFile == "/tmp/cmu_indic_guj_ad.flitevox")
        maxSize = 11;
    else if (downloadFile == "/tmp/cmu_indic_guj_dp.flitevox")
        maxSize = 11;
    else if (downloadFile == "/tmp/cmu_indic_guj_kt.flitevox")
        maxSize = 5;
    else if (downloadFile == "/tmp/cmu_indic_hin_ab.flitevox")
        maxSize = 33;
    else if (downloadFile == "/tmp/cmu_indic_kan_plv.flitevox")
        maxSize = 9;
    else if (downloadFile == "/tmp/cmu_indic_mar_aup.flitevox")
        maxSize = 11;
    else if (downloadFile == "/tmp/cmu_indic_mar_slp.flitevox")
        maxSize = 11;
    else if (downloadFile == "/tmp/cmu_indic_pan_amp.flitevox")
        maxSize = 9;
    else if (downloadFile == "/tmp/cmu_indic_tam_sdr.flitevox")
        maxSize = 11;
    else if (downloadFile == "/tmp/cmu_indic_tel_kpn.flitevox")
        maxSize = 11;
    else if (downloadFile == "/tmp/cmu_indic_tel_sk.flitevox")
        maxSize = 11;
    else if (downloadFile == "/tmp/cmu_indic_tel_ss.flitevox")
        maxSize = 25;
}

void progressDialog::checkDownloadFileSize()
{
    int size = 0;
    QFile myFile(downloadFile);
    if (myFile.open(QIODevice::ReadOnly)){
        size = myFile.size();  //when file does open.
        myFile.close();
    }
    float sizeInMegabytes = size / 1000000;
    int percent = (sizeInMegabytes / maxSize) * 100;
    progressBar->setValue(percent);
}

void progressDialog::stopTimer()
{
    timer.stop();
}

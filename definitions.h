#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#include <QString>
#include <QStandardPaths>
#include <QDir>

#define KalFestival "Kal Festival"
#define AwbCmuFestival "Awb Cmu Festival"
#define AewCmuFestival "Aew Cmu Festival"
#define AupCmuFestival "Aup Cmu Festival"
#define AhwCmuFestival "Ahw Cmu Festival"
#define AxbCmuFestival "Axb Cmu Festival"
#define BdlCmuFestival "Bdl Cmu Festival"
#define ClbCmuFestival "Clb Cmu Festival"
#define EeyCmuFestival "Eey Cmu Festival"
#define FemCmuFestival "Fem Cmu Festival"
#define GkaCmuFestival "Gka Cmu Festival"
#define JmkCmuFestival "Jmk Cmu Festival"
#define KspCmuFestival "Ksp Cmu Festival"
#define LjmCmuFestival "Ljm Cmu Festival"
#define LnhCmuFestival "Lnh Cmu Festival"
#define RmsCmuFestival "Rms Cmu Festival"
#define RxrCmuFestival "Rxr Cmu Festival"
#define SlpCmuFestival "Slp Cmu Festival"
#define SltCmuFestival "Slt Cmu Festival"
#define CustomFestival "Custom Festival"
#define RmsFlite "Rms Flite"
#define SltFlite "Slt Flite"
#define AwbFlite "Awb Flite"
#define Kal16Flite "Kal16 Flite"
#define IndicBenRmFlite "Indic Ben Rm Flite"
#define IndicGujAdFlite "Indic Guj Ad Flite"
#define IndicGujDpFlite "Indic Guj Dp Flite"
#define IndicGujKtFlite "Indic Guj Kt Flite"
#define IndicHinAbFlite "Indic Hin Ab Flite"
#define IndicKanPlvFlite "Indic Kan Plv Flite"
#define IndicMarAupFlite "Indic Mar Aup Flite"
#define IndicMarSlpFlite "Indic Mar Slp Flite"
#define IndicPanAmpFlite "Indic Pan Amp Flite"
#define IndicTamSdrFlite "Indic Tam Sdr Flite"
#define IndicTelKpnFlite "Indic Tel Kpn Flite"
#define IndicTelSkFlite "Indic Tel Sk Flite"
#define IndicTelSsFlite "Indic Tel Ss Flite"
#define AewFlite "Aew Flite"
#define AhwFlite "Ahw Flite"
#define AupFlite "Aup Flite"
#define Awb2Flite "Awb2 Flite"
#define AxbFlite "Axb Flite"
#define BdlFlite "Bdl Flite"
#define ClbFlite "Clb Flite"
#define EeyFlite "Eey Flite"
#define FemFlite "Fem Flite"
#define GkaFlite "Gka Flite"
#define JmkFlite "Jmk Flite"
#define KspFlite "Ksp Flite"
#define LjmFlite "Ljm Flite"
#define LnhFlite "Lnh Flite"
#define Rms2Flite "Rms2 Flite"
#define RxrFlite "Rxr Flite"
#define SlpFlite "Slp Flite"
#define Slt2Flite "Slt2 Flite"
#define ItalianMary "Mary Italian"
#define GermanMary "Mary German"
#define TurkishMary "Mary Turkish"
#define GreekMary "Mary Greek"
#define GreekHercules "Mary Hercules Greek"
#define GreekGoogleMary "Google Mary Greek"
#define FrenchMary "Mary French"
#define CmuBdlMary "Mary Cmu Bdl"
#define CmuRmsMary "Mary Cmu Rms"
#define CmuSltMary "Mary Cmu Slt"
#define ObadiahMary "Mary Obadiah"
#define PoppyMary "Mary Poppy"
#define PrudenceMary "Mary Prudence"
#define SpikeMary "Mary Spike"
#define LessacLowPiper "Lessac Low Piper"

#define defDurationStretch 100
#define defTargetMean 100

const QString wavFile = "/tmp/omilo.wav";
const QString textFile = "/tmp/omilo.txt";
const QString voiceFile = "/tmp/omiloVoice.txt";
const QString voiceTextEditorFile = "/tmp/omiloTextEditorVoice.txt";
const QString showCommandFile = "/tmp/omiloShowcommand.txt";
const QString silenceFile = "/tmp/omilo-silence.wav";
const unsigned int maxMaryMemory = 2048;
const unsigned int minMaryMemory = 350;

const QString defaultFestivalCommand = "/usr/bin/text2wave";
const QString defaultFestivalCommandArguments = "-eval '(voice_kal_diphone)'";

const QString tempPath = QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory);
const QString testFile = tempPath + "omilo-test.wav";
const QString wavPrefix = tempPath + "omilo-";
const QString tempFile = tempPath + "omilo-tmp.wav";
const QString expPrefix = tempPath + "omilo-exp-";
const QString expTempFile = tempPath + "omilo-exp-tmp.wav";

#ifdef Q_OS_WIN
const QString maryHttpAddress = "http://localhost:59125/process?INPUT_TEXT=";
const QString soxCommand = "\"" + QDir::currentPath() + "/sox/sox.exe\"";
const QString fliteCommand = "\"" + QDir::currentPath() + "/flite.exe\"";
const QString dummyCommand = soxCommand;
const QString maryInstallationDir = QDir::currentPath() + "\\marytts\\lib\\";
#define defaultVoice LessacLowPiper;

#else
const QString maryHttpAddress = "http://localhost:59127/process?INPUT_TEXT=";
const QString soxCommand = "sox ";
const QString dummyCommand = "echo foo";
const QString fliteCommand = "/usr/share/omilo-qt5/flite/flite";
const QString maryInstallationDir = "/usr/share/omilo-qt5/marytts-5.0";
#define defaultVoice LessacLowPiper;
#endif

#define maximumNumberOfFiles 1000000

enum engineMode
{
    unknown = -1,
    festival = 0,
    festivalCmu = 1,
    flite = 2,
    marytts = 3,
    piper = 4
};

struct VoiceInfo
{
    engineMode mode;
    QString name;
    QString language;
};

class TextProcessItem
{
public:
    unsigned int id;
    QString text;
    unsigned int begin;
    unsigned int end;
    QString filename;
};

#endif // DEFINITIONS_H
